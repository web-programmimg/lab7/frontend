import { ref } from 'vue'
import { defineStore } from 'pinia'
import userService from '@/services/user'
import type { User } from '@/types/User'

export const useUserStore = defineStore('user', () => {
  const users = ref<User[]>([])

  async function getUsers() {
    // loadingStore.doLoad()
    const res = await userService.getUsers()
    users.value = res.data
    // loadingStore.finish()
  }

  async function getUser(id: number) {
    // loadingStore.doLoad()
    const res = await userService.getUser(id)
    users.value = res.data
    // loadingStore.finish()
  }

  async function saveUser(user: User) {
    if (user.id < 0) {
      //Add new
      const res = await userService.addNew(user)
    } else {
      // Update
      const res = await userService.updateUser(user)
    }
    await getUsers()
  }

  async function deleteUser(user: User) {
    const res = await userService.delUser(user)
    await getUsers()
  }
  return { users, getUser, getUsers, deleteUser, saveUser }
})
