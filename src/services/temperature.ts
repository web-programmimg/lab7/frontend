import http from './http'

type ReturnData = {
  celsius: number
  fahrenheit: number
}

async function convert(celsius: number): Promise<number> {
  console.log('Service: Call Convert')
  console.log('/temperature/convert?celsius=' + celsius.value)
  const res = await http.get('/temperature/convert?celsius=' + celsius.value)

  const convertResult: ReturnData = res.data
  console.log('Service: Finish Call Convert')
  return convertResult.fahrenheit
}

export default { convert }
